#!/usr/bin/env python3
import io
import torch
import torch.nn as nn
import numpy as np
from realnvp import AbstractCoupling
from unet import UNetAdd
import matplotlib.pyplot as plt
from torchvision.transforms import ToTensor
import PIL.Image


def logit(x):
    output = torch.log(1 / (1 - torch.exp(-x)))
    if not torch.isfinite(output).all():
        import pdb; pdb.set_trace()
    return output

def logit_derivative(x):
    output = 1 / (1 - torch.exp(x))
    if not torch.isfinite(output).all():
        import pdb; pdb.set_trace()
    return output

def sigmoid(x):
    output = 1 / (1 + torch.exp(-x))
    if not torch.isfinite(output).all():
        import pdb; pdb.set_trace()
    return output

def sigmoid_derivative(x):
    sigma = sigmoid(x)
    output = sigma * (1 - sigma)
    if not torch.isfinite(output).all():
        import pdb; pdb.set_trace()
    return output

class ImageRealNVP(nn.Module):
    def __init__(self, prior, hps, device):
        """Initializes a RealNVP.

        Args:
            prior: prior distribution over latent space Z.
            hps: the set of hyperparameters.
        """
        super(ImageRealNVP, self).__init__()
        self.prior = prior
        self.hps = hps
        self.device = device

        in_out_dim = 2
        mid_dim = 256
        dim = hps.base_dim

        self.layers = nn.ModuleList([
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
        ])

        """self.layers = nn.ModuleList([
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 0., hps),
            ImageChannelwiseCoupling(in_out_dim, mid_dim, 1., hps),
        ])"""

    def g(self, z):
        """Transformation g: Z -> X (inverse of f).

        Args:
            z: tensor in latent space Z.
        Returns:
            transformed tensor in data space X.
        """
        x, log_diag_J = z.clone(), torch.zeros_like(z)
        for i in reversed(range(len(self.layers))):
            x, inc = self.layers[i](x, reverse=True)
            log_diag_J = log_diag_J + inc

        return x, log_diag_J

    def f(self, x):
        """Transformation f: X -> Z (inverse of g).

        Args:
            x: tensor in data space X.
        Returns:
            transformed tensor and log of diagonal elements of Jacobian.
        """
        z, log_diag_J = x.clone(), torch.zeros_like(x)

        for i in range(len(self.layers)):
            z, inc = self.layers[i](z)
            log_diag_J = log_diag_J + inc

        return z, log_diag_J

    def log_prob(self, x):
        """Computes data log-likelihood.

        (See Eq(2) and Eq(3) in the real NVP paper.)

        Args:
            x: input minibatch.
        Returns:
            log-likelihood of input.
        """
        z, log_diag_J = self.f(x)
        log_det_J = torch.sum(log_diag_J, dim=(1, 2, 3))
        log_prior_prob = torch.sum(self.prior.log_prob(z), dim=(1, 2, 3))
        return log_prior_prob + log_det_J

    def log_prob_gen(self, z):
        x, log_diag_J = self.g(z)
        log_det_J = torch.sum(log_diag_J, dim=(1, 2, 3))
        log_prior_prob = torch.sum(self.prior.log_prob(z), dim=(1, 2, 3))
        return x, -log_det_J


    def write_latent_space(self, device, epoch, writer, dims):
        width = dims[0]
        height = dims[1]
        output = {"x": [], "y": [], "z": []}

        for x in range(width):
            data = np.zeros((width, 2))
            for y in range(height):
                data[y, 0] = x / (width - 1)
                data[y, 1] = y / (height - 1)
            data = torch.from_numpy(data).reshape((height, 2, 1, 1)).to(device).float()

            z = self.f(data)[0].squeeze().detach().cpu().numpy()
            log_px = self.log_prob(data)
            px = np.exp(log_px.squeeze().detach().cpu().numpy())

            output["x"].extend(z[:,0].tolist())
            output["y"].extend(z[:,1].tolist())
            output["z"].extend(px.tolist())

        plt.scatter(output["x"], output["y"], c=output["z"])
        plt.title("test")
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)
        plot_buf = buf
        image = PIL.Image.open(plot_buf)
        image = ToTensor()(image)

        writer.add_image("Latent_Space", image, epoch)
        plt.close()


    def sample(self, size, device, prior, dims):
        """Generates samples.

        Args:
            size: number of samples to generate.
        Returns:
            samples from the data space X.
        """
        width = dims[0]
        height = dims[1]
        output = np.zeros((width, height))
        for x in range(width):
            data = np.zeros((width, 2))
            for y in range(height):
                data[y, 0] = x / (width - 1)
                data[y, 1] = y / (height - 1)
            data = torch.from_numpy(data).reshape((height, 2, 1, 1)).to(device).float()

            log_px = self.log_prob(data)
            px = np.exp(log_px.squeeze().detach().cpu().numpy())

            output[x] = px

        return output

    def forward(self, x):
        """Forward pass.

        Args:
            x: input minibatch.
        Returns:
            log-likelihood of input and sum of squares of scaling factors.
            (the latter is used in L2 regularization.)
        """
        weight_scale = None
        for name, param in self.named_parameters():
            param_name = name.split('.')[-1]
            if param_name in ['weight_g', 'scale'] and param.requires_grad:
                if weight_scale is None:
                    weight_scale = torch.pow(param, 2).sum()
                else:
                    weight_scale = weight_scale + torch.pow(param, 2).sum()
        return self.log_prob(x), weight_scale


class ImageChannelwiseCoupling(nn.Module):
    def __init__(self, in_out_dim, mid_dim, mask_config, hps):
        """Initializes a ImageChannelwiseCoupling.

        Args:
            in_out_dim: number of input and output features.
            mid_dim: number of features in residual blocks.
            mask_config: 1 if change the top half, 0 if change the bottom half.
            hps: the set of hyperparameters.
        """
        super(ImageChannelwiseCoupling, self).__init__()

        self.coupling = ImageChannelwiseAffineCoupling(
                in_out_dim, mid_dim, mask_config, hps)

    def forward(self, x, reverse=False):
        """Forward pass.

        Args:
            x: input tensor.
            reverse: True in inference mode, False in sampling mode.
        Returns:
            transformed tensor and log of diagonal elements of Jacobian.
        """
        return self.coupling(x, reverse)


class ImageChannelwiseAffineCoupling(AbstractCoupling):
    def __init__(self, in_out_dim, mid_dim, mask_config, hps):
        """Initializes a ChannelwiseAffineCoupling.

        Args:
            in_out_dim: number of input and output features.
            mid_dim: number of features in residual blocks.
            mask_config: 1 if change the top half, 0 if change the bottom half.
            hps: the set of hyperparameters.
        """
        super(ImageChannelwiseAffineCoupling, self).__init__(mask_config, hps)

        self.coupling_bn = False

        self.scale = nn.Parameter(torch.zeros(1), requires_grad=True)
        self.scale_shift = nn.Parameter(torch.zeros(1), requires_grad=True)

        network = UNetAdd(in_out_dim, in_out_dim)

        self.block = nn.Sequential(        # 1st half of resnet: shift
            #nn.ReLU(),                    # 2nd half of resnet: log_rescale
            network)

    def forward(self, x, reverse=False):
        """Forward pass.

        Args:
            x: input tensor.
            reverse: True in inference mode, False in sampling mode.
        Returns:
            transformed tensor and log of diagonal elements of Jacobian.
        """
        [_, C, _, _] = list(x.size())
        if self.mask_config:
            (on, off) = x.split(C//2, dim=1)
        else:
            (off, on) = x.split(C//2, dim=1)
        off_ = off
        off_ = torch.cat((off_, -off_), dim=1)     # C channels
        out = self.block(off_)
        (shift, log_rescale) = out.split(C//2, dim=1)
        log_rescale = self.scale * torch.tanh(log_rescale) + self.scale_shift

        log_diag_J = log_rescale     # See Eq(6) in real NVP
        # See Eq(7) and Eq(8) and Section 3.7 in real NVP
        if reverse:
            on = (on - shift) * torch.exp(-log_rescale)
        else:
            on = on * torch.exp(log_rescale) + shift
        if self.mask_config:
            x = torch.cat((on, off), dim=1)
            log_diag_J = torch.cat((log_diag_J, torch.zeros_like(log_diag_J)), 
                dim=1)
        else:
            x = torch.cat((off, on), dim=1)
            log_diag_J = torch.cat((torch.zeros_like(log_diag_J), log_diag_J), 
                dim=1)

        return x, log_diag_J