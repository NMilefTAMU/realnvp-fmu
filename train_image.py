#!/usr/bin/env python3
import argparse
import time
import os

import torch, torchvision
import torch.distributions as distributions
import torch.optim as optim
import torchvision.utils as utils

from utils import parse_arguments
from utils import Hyperparameters, UniformCustom
import numpy as np
import realnvp, data_utils, image_realnvp

from torch.utils.tensorboard import SummaryWriter

from PIL import Image
from image_dataset import ImageDataset

def main(args):
    device = torch.device("cuda:" + str(args.gpu))
    image_name = args.image.split("/")[-1].split(".png")[0]
    experiment_name = time.time() * 1000
    writer = SummaryWriter("runs/" + image_name + "_" + str(experiment_name))

    # model hyperparameters
    dataset = args.dataset
    batch_size = args.batch_size
    hps = Hyperparameters(
        base_dim = args.base_dim, 
        res_blocks = args.res_blocks, 
        bottleneck = args.bottleneck, 
        skip = args.skip, 
        weight_norm = args.weight_norm, 
        coupling_bn = args.coupling_bn, 
        affine = args.affine,
        network_type = "unet")
    scale_reg = 5e-5    # L2 regularization strength 

    # optimization hyperparameters
    lr = args.lr
    momentum = args.momentum
    decay = args.decay

    # prefix for images and checkpoints
    filename = 'bs%d_' % batch_size \
             + 'normal_' \
             + 'bd%d_' % hps.base_dim \
             + 'rb%d_' % hps.res_blocks \
             + 'bn%d_' % hps.bottleneck \
             + 'sk%d_' % hps.skip \
             + 'wn%d_' % hps.weight_norm \
             + 'cb%d_' % hps.coupling_bn \
             + 'af%d' % hps.affine \

    # load dataset
    train_split = ImageDataset(args)
    train_loader = torch.utils.data.DataLoader(train_split,
        batch_size=batch_size, shuffle=True)#, num_workers=2)
    val_split = ImageDataset(args)
    val_loader = torch.utils.data.DataLoader(val_split,
        batch_size=batch_size, shuffle=False)#, num_workers=2)

    #prior = distributions.Normal(   # isotropic standard normal distribution
        #torch.tensor(0.).to(device), torch.tensor(1.).to(device))

    prior = distributions.Normal(torch.tensor(0.5).to(device), torch.tensor(1.).to(device))
    #prior = distributions.Uniform(torch.tensor(0.0).to(device), torch.tensor(1.0).to(device))

    flow = image_realnvp.ImageRealNVP(prior=prior, hps=hps, device=device).to(device)
    optimizer = optim.Adamax(flow.parameters(), lr=lr, betas=(momentum, decay), eps=1e-7)
    
    epoch = 0
    running_loss = 0.
    running_log_ll = 0.
    optimal_log_ll = float('-inf')
    early_stop = 0

    #image_size = data_info.channel * data_info.size**2    # full image dimension

    loss_function = torch.nn.KLDivLoss(reduction="batchmean")

    while epoch < args.max_epoch:
        epoch += 1
        print('Epoch %d:' % epoch)
        flow.train()
        for batch_idx, data in enumerate(train_loader, 1):
            optimizer.zero_grad()

            x, y = data

            x = x.to(device).float()
            y = y.to(device).float()

            # log-likelihood of input minibatch
            log_prob = flow.log_prob(x)

            # add L2 regularization on scaling factors
            loss = loss_function(log_prob, y.squeeze())
            running_loss += loss.item()

            loss.backward()
            optimizer.step()

            #if batch_idx % 10 == 0:
                #bit_per_dim = (-log_ll.item() + np.log(256.) * image_size) \
                    #/ (image_size * np.log(2.))
                #print('[%d/%d]\tloss: %.3f\tlog-ll: %.3f\tbits/dim: %.3f' % \
                    #(batch_idx*batch_size, len(train_loader.dataset), 
                        #loss.item(), log_ll.item(), bit_per_dim))

        mean_loss = running_loss / batch_idx
        #mean_log_ll = running_log_ll / batch_idx
        #mean_bit_per_dim = (-mean_log_ll + np.log(256.) * image_size) \
             #/ (image_size * np.log(2.))
        print('===> Average train loss: %.3f' % mean_loss)
        writer.add_scalar("Loss", mean_loss, epoch)
        #print('===> Average train log-likelihood: %.3f' % mean_log_ll)
        #print('===> Average train bit_per_dim: %.3f' % mean_bit_per_dim)
        running_loss = 0.
        #running_log_ll = 0.

        flow.eval()
        if epoch % 10 == 0:
        #with torch.no_grad():
            for batch_idx, data in enumerate(val_loader, 1):
                x, y = data
                x = x.to(device).float()
                y = y.to(device).float()

                # log-likelihood of input minibatch
                log_prob = flow.log_prob(x)

                # add L2 regularization on scaling factors
                loss = loss_function(log_prob, y.squeeze())
                running_loss += loss.item()

            mean_loss = running_loss / batch_idx
            #mean_log_ll = running_log_ll / batch_idx
            #mean_bit_per_dim = (-mean_log_ll + np.log(256.) * image_size) \
                #/ (image_size * np.log(2.))
            print('===> Average validation loss: %.3f' % mean_loss)
            #print('===> Average validation log-likelihood: %.3f' % mean_log_ll)
            #print('===> Average validation bits/dim: %.3f' % mean_bit_per_dim)
            running_loss = 0.
            #running_log_ll = 0.

            samples = flow.sample(args.sample_size, device, prior, train_split.get_dims())
            flow.write_latent_space(device, epoch, writer, train_split.get_dims())
            save_image(args, samples, epoch, writer)
            #samples, _ = data_utils.logit_transform(samples, reverse=True)
            #utils.save_image(samples,
                #'./samples_nis/' + dataset + '/' + filename + '_ep%d.png' % epoch)

        #if mean_log_ll > optimal_log_ll:
            #early_stop = 0
            #optimal_log_ll = mean_log_ll
            #torch.save(flow, './models/' + dataset + '/' + filename + '.model')
            #print('[MODEL SAVED]')
        #else:
            #early_stop += 1
            #if early_stop >= 100:
                #break
        
        #print('--> Early stopping %d/100 (BEST validation log-likelihood: %.3f)' \
            #% (early_stop, optimal_log_ll))

    print('Training finished at epoch %d.' % epoch)

def save_image(args, image, epoch, writer):
    os.makedirs("samples_image" + str(args.gpu) + "/", exist_ok=True)
    print(np.min(image), np.max(image))
    image_src= np.array(Image.open(args.image)) / 255.0
    mean = np.mean(image_src[:,:,0])
    image = np.minimum(np.maximum(image * mean, 0.0), 1.0)# * 255.0
    #img = (Image.fromarray((image).astype(np.uint8), "L"))
    #img.save("samples_image" + str(args.gpu) + "/image_" + str(epoch) + ".png")

    output_image = np.stack((image, image, image), axis=0)
    writer.add_image("Output", output_image, epoch)

if __name__ == "__main__":
    main(parse_arguments())