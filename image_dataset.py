#!/usr/bin/env python3
import numpy as np
from PIL import Image

import torch
from torch.utils.data import Dataset

class ImageDataset(Dataset):
    def __init__(self, args):
        image = np.array(Image.open(args.image)) / 255.0
        mean = np.mean(image[:, :, 0])
        self.image_data = np.zeros((image.shape[0] * image.shape[1], 3))
        self.image_dim = (image.shape[0], image.shape[1])

        count = 0
        for x in range(image.shape[0]):
            for y in range(image.shape[1]):
                self.image_data[count, 0] = x / (image.shape[0] - 1)
                self.image_data[count, 1] = y / (image.shape[1] - 1)
                self.image_data[count, 2] = image[x, y, 0] / mean
                count += 1

    def __getitem__(self, i):
        x = torch.from_numpy(self.image_data[i, 0:2])
        y = torch.from_numpy(self.image_data[i, 2:])

        x = x.reshape((x.shape[0], 1, 1))
        y = y.reshape((y.shape[0], 1, 1))

        return x, y

    def __len__(self):
        return self.image_data.shape[0]

    def get_dims(self):
        return self.image_dim