#!/usr/bin/env python3
import torch
import torch.nn as nn

class UNetAdd(nn.Module):
    """
    UNet class from Neural Importance Sampling paper
    """
    def __init__(self, input_channels, output_channels):
        super(UNetAdd, self).__init__()

        self.nn = nn.ModuleList()
        self.nn.append(nn.Linear(input_channels, 256))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(256, 128))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(128, 64))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(64, 32))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(32, 16))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(16, 32))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(32, 64))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(64, 128))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(128, 256))
        self.nn.append(nn.ReLU())
        self.nn.append(nn.Linear(256, output_channels))

    def forward(self, x):
        x_r = x.reshape((x.shape[0], x.shape[1]))
        x_d_256   = self.nn[0](x_r)
        x_d_256a  = self.nn[1](x_d_256)
        x_d_128   = self.nn[2](x_d_256a)
        x_d_128a  = self.nn[3](x_d_128)
        x_d_64    = self.nn[4](x_d_128a)
        x_d_64a   = self.nn[5](x_d_64)
        x_d_32    = self.nn[6](x_d_64a)
        x_d_32a   = self.nn[7](x_d_32)
        x_d_16    = self.nn[8](x_d_32a)
        x_d_16a   = self.nn[9](x_d_16)
        x_u_32    = self.nn[10](x_d_16a) + x_d_32
        x_u_32a   = self.nn[11](x_u_32)
        x_u_64    = self.nn[12](x_u_32a) + x_d_64
        x_u_64a   = self.nn[13](x_u_64)
        x_u_128   = self.nn[14](x_u_64a) + x_d_128
        x_u_128a  = self.nn[15](x_u_128)
        x_u_256   = self.nn[16](x_u_128a) + x_d_256
        x_u_256a  = self.nn[17](x_u_256)
        output  = self.nn[18](x_u_256a)
        output_r = output.reshape((output.shape[0], output.shape[1], 1, 1))
        return output_r
